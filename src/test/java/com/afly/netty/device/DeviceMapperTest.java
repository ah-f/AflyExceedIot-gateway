package com.afly.netty.device;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.afly.netty.business.domain.Device;
import com.afly.netty.business.mapper.DeviceMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * <p>  </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/28 11:44
 */
@Slf4j
@SpringBootTest
public class DeviceMapperTest {

    @Autowired
   private DeviceMapper deviceMapper;


    @Test
    public void test_query(){
        log.info("devices===>{}");
        List<Device> devices = deviceMapper.selectList(null);
        log.info("devices===>{}",devices);
    }

    @Test
    public void test_query1(){
        System.out.println("aaa");
    }


    @Test
    public void test_insert_history_online(){
        Device device = Device.builder().number("123456789")
                .lastOnline(LocalDateTimeUtil.now())
                .status(0).build();
        int i = deviceMapper.insertDeviceOnlineHistory(device);
        log.info("i===>{}",i);
    }


}
