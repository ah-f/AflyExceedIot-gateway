package com.afly.netty.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * <p> 自定义线程池 </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/28 15:53
 */
@Configuration
public class ThreadPoolConfiguration {

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor poolExecutor = new ThreadPoolTaskExecutor();
        // 核心线程数
        poolExecutor.setCorePoolSize(5);
        // 最大线程数
        poolExecutor.setMaxPoolSize(15);
        // 队列大小
        poolExecutor.setQueueCapacity(100);
        // 线程最大空闲时间
        poolExecutor.setKeepAliveSeconds(300);
        // 拒绝策略
        poolExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 线程名称前缀
        poolExecutor.setThreadNamePrefix("service-pool-");

        return poolExecutor;
    }

}
