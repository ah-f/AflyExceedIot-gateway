package com.afly.netty.core.codec;

import com.afly.netty.util.HexUtil;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import lombok.extern.slf4j.Slf4j;

/**
 * <p>  </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/28 10:42
 */
@Slf4j
public class DeviceEncoder extends MessageToByteEncoder<String> {
    @Override
    protected void encode(ChannelHandlerContext ctx, String in, ByteBuf out) throws Exception {
        log.info("in===>{}", in);
        byte[] bytes = HexUtil.hexToByteArray(in.replace(" ",""));
        out.writeBytes(bytes);
    }
}
