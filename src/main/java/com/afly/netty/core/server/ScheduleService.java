package com.afly.netty.core.server;

import com.afly.netty.util.CacheUtil;
import io.netty.channel.Channel;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class ScheduleService {
    @Scheduled(cron = "0/5 * * * * ? ") //每5秒钟下发查询指令
    //@Scheduled(cron = "0 0/3 * * * ? ") //三分钟下发查询指令
    public void readDeviceTask() {
        Collection<Channel> channels = CacheUtil.cacheClientChannel.values();
        channels.parallelStream().forEach((channel) ->{
            channel.writeAndFlush("110200200001BA90");
            System.out.println("下发指令成功");
        });
    }
}
