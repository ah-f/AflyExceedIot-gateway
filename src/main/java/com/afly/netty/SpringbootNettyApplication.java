package com.afly.netty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class SpringbootNettyApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootNettyApplication.class, args);
    }
}
