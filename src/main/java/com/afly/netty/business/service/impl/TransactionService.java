package com.afly.netty.business.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.afly.netty.business.domain.Transaction;
import com.afly.netty.business.mapper.TransactionMapper;
import com.afly.netty.business.service.ITransactionService;
import org.springframework.stereotype.Service;

@Service
public class TransactionService extends ServiceImpl<TransactionMapper, Transaction> implements ITransactionService {
}
