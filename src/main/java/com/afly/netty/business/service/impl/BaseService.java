package com.afly.netty.business.service.impl;

import com.afly.netty.business.domain.Device;
import com.afly.netty.business.mapper.DeviceMapper;
import com.afly.netty.business.service.IBaseService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * <p> 注入spring管理的数据层基础类 </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/28 15:20
 */
@Component
public class BaseService implements IBaseService {

    @Resource
    private DeviceMapper deviceMapper;

    @Override
    public void save(Device device) {
        deviceMapper.insert(device);
    }
}
