package com.afly.netty.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.afly.netty.business.domain.Transaction;

public interface ITransactionService extends IService<Transaction> {
}
