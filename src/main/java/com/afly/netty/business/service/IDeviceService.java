package com.afly.netty.business.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.afly.netty.business.domain.Device;

public interface IDeviceService extends IService<Device> {


    /**
     *  修改设备上下线状态
     * @param device
     * @return
     */
    Boolean updateDeviceStatus(Device device);
}
