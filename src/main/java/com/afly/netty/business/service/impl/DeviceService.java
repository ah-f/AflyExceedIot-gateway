package com.afly.netty.business.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.afly.netty.business.domain.Device;
import com.afly.netty.business.mapper.DeviceMapper;
import com.afly.netty.business.service.IDeviceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class DeviceService extends ServiceImpl<DeviceMapper, Device> implements IDeviceService {
    private final DeviceMapper deviceMapper;

    @Override
    @Transactional
    public Boolean updateDeviceStatus(Device device) {
        Device deviceBase = deviceMapper.selectOne(new QueryWrapper<Device>().lambda().eq(Device::getNumber, device.getNumber()));
        if (deviceBase == null) {
            return false;
        }
        deviceMapper.update(null, new UpdateWrapper<Device>().lambda()
                .eq(Device::getNumber, device.getNumber())
                .set(Device::getStatus, device.getStatus())  //设置为离线状态
                .set(Device::getLastOnline, device.getLastOnline()) //设置离线时间
        );
        //添加设备上线记录
        int result = deviceMapper.insertDeviceOnlineHistory(device);
        return result > 0;
    }
}
