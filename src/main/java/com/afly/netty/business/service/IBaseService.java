package com.afly.netty.business.service;

import com.afly.netty.business.domain.Device;

/**
 * <p>  </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/28 15:22
 */
public interface IBaseService {


    void save(Device device);

}
