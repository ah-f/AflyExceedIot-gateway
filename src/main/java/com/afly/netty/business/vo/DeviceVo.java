package com.afly.netty.business.vo;

import lombok.Data;

import java.util.Date;

@Data
public class DeviceVo {
    private Integer id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备编号
     */
    private String number;
    /**
     * 设备状态   1:在线  0:离线
     */
    private Integer status;
    /**
     * 设备地址
     */
    private String address;
    /**
     * 设备坐标
     */
    private String coordinate;
    /**
     * 最后在线时间
     */
    private Date lastOnline;
}
