package com.afly.netty.business.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Create by liuzm
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Device implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备编号
     */
    private String number;
    /**
     * 设备状态   1:在线  0:离线
     */
    private Integer status;
    /**
     * 设备地址
     */
    private String address;

    /**
     * 最后在线时间
     */
    private LocalDateTime lastOnline;

}
