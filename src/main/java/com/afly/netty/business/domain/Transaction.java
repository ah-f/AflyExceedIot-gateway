package com.afly.netty.business.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @TableName iot_transaction
 */
@Data
@Builder
public class Transaction implements Serializable {

    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;
    /**
     * 设备编号
     */
    private String deviceNo;
    /**
     * 接收数据
     */
    private String data;
    /**
     * 来自(0主动 1查询)
     */
    private String fromType;
    /**
     * 接收时间
     */
    private LocalDateTime time;

}
