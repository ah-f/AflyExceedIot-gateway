package com.afly.netty.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.afly.netty.business.domain.Transaction;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TransactionMapper extends BaseMapper<Transaction> {
}
