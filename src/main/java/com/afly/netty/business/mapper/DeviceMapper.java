package com.afly.netty.business.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.afly.netty.business.domain.Device;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DeviceMapper extends BaseMapper<Device> {


    int insertDeviceOnlineHistory(Device device);

}
