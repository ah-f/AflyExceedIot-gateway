package com.afly.netty.business.dto;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class DeviceDto {
    /**
     * 设备名称
     */
    @NotNull(message = "设备名称不能为空")
    @Length(min = 1, max = 20, message = "设备名称长度不能超过20")
    private String name;
    /**
     * 设备编号
     */
    @NotNull(message = "设备编号不能为空")
    private String number;
    /**
     * 设备状态   1:在线  0:离线
     */
    private Integer status;
    /**
     * 设备地址
     */
    @NotNull(message = "设备地址不能为空")
    private String address;
    /**
     * 设备坐标
     */
    @NotNull(message = "设备坐标不能为空")
    private String coordinate;
}
