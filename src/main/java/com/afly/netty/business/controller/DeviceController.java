package com.afly.netty.business.controller;

import com.afly.netty.business.domain.EasyResult;
import com.afly.netty.business.dto.DeviceDto;
import com.afly.netty.business.pagination.DevicePagination;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.afly.netty.business.domain.Device;
import com.afly.netty.business.service.IDeviceService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p> 设备相关接口 </p>
 *
 * @author liuziming
 * @description
 * @date 2023/10/26 11:35
 */
@Slf4j
@RestController
@RequestMapping("/device")
@AllArgsConstructor
public class DeviceController {

    @Resource
    private IDeviceService deviceService;

    //查询设备
    @GetMapping("/list")
    public EasyResult list(@RequestParam DevicePagination devicePagination) {
        IPage<Device> page = new Page<>(devicePagination.getCurrent(), devicePagination.getLimit());
        IPage<Device> deviceIPage = deviceService.page(page, new QueryWrapper<Device>().lambda()
                .eq(devicePagination.getNumber() != null, Device::getNumber, devicePagination.getNumber()));
        return EasyResult.buildSuccessResult(deviceIPage.getRecords());
    }

    /**
     * 添加设备
     *
     * @param deviceDto
     * @return
     */
    @PostMapping("/add")
    public EasyResult add(@Validated @RequestBody DeviceDto deviceDto) {
        Device device = new Device();
        BeanUtils.copyProperties(deviceDto, device);
        deviceService.save(device);
        return EasyResult.buildSuccessResult();
    }


}
