package com.afly.netty.business.pagination;

import lombok.Data;

@Data
public class DevicePagination {
    // 当前的页码
    private int current = 1;
    // 单页显示数量
    private int limit = 10;

    private String number;    // 设备编号
}
